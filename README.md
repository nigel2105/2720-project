# CSCI2720 Project

## Guideline of Repository

### data_processing 
Contains the files related to given government data, specification, and code to process the data and save it into database

* Data_Specification_for_Fund-Raising_Activities_PSI_EN.pdf: Specific data given by the government
* event_sample.js: Sample to store events recorded to database.
* fundraising.json: Official document from the government about events and its corresponding details
* process.js: Parsing data from fundraising.json and then parse the stored event data whilst picking important event details

### Project_Spec_folder
Contains the specification of project ( can be found in blackboard) with useful references 

### Schema
Contains the scheme of objects used to perform CRUD operations, based on mongoose  

* EventSchema.js: Contains the schema of object type Event, exports Event schema
* UserSchema.js: Contains the schema of object type User, exports User schema
* CommentSchema.js: contains the schema of type Comments, exports Comment schema

### public 
---> contains the static content of websites, HTML, CSS as well as dynamics: Javascript.

#### CSS
Styling function.

* signin.css : stylesheet for the sign in page

#### HTML
Containing the webpages 

* eventdetails: when you click each event to show the  detail
* events: Upon clicking events in navbar, and shows all the events stored in database
* index: landing page/ home page
* search_event: after entering form in search_form , show all events relevant to it
* search_form: form given to prompt you for tag and keyword
* signin: form for sign in 
* signup: form for sign up

#### img
contains the images relevant to use in pages

#### javascripts
the dynamic scripts for handling pages


### Root
* routing.js: handling all CRUD operations, dealing with database