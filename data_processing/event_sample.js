// prototype storing to database
var getData = require('./process.js');
//console.log(getData[0]); example of use  getData
var Event = require('../Schema/EventSchema');
var mongoose = require('mongoose');

mongoose.connect('mongodb://nigel:nigel@localhost/project');
var db= mongoose.connection;

db.on('error',console.error.bind(console,
    "Connection error:"));

db.once('open', function(){
    console.log("Connection is open...");
});

var e = new Event({
    name: getData[1].activity_name,
    time: getData[1].date_and_time,
    orgName: getData[1].org_name,
    venue: getData[1].venue,
    contact: getData[1].contact,
});

e.save(function(err){
    if(err)
        console.log(err);
})