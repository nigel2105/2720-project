// Code to organise the data processing from given .JSON provided by data.gov.hk
var fs = require('fs');
var data= fs.readFileSync('fundraising.json', 'utf8');
var content = JSON.parse(data);
var bodyparser = require('body-parser');

var activity_list = content.activities; // activites now contain 
// activity_list.length = 10

/** 4 Criteria needed to store
 *  - Title/Activity Name/Program Name
 *  - Program Date and Time( single piece of string)
 *  - Organization Name
 *  - Venue Name
 *  - Enquiry Contact ( EXTRA)
 */
var activity = Array();
for(i=0 ; i<activity_list.length; i++) {
        var x ={
            activity_name: activity_list[i].activityNameEnglish,
            date_and_time: activity_list[i].schedule[0].dateFrom +" " +activity_list[i].schedule[0].timeFrom,  // for date_and_time, we choose the latest event
            org_name: activity_list[i].organisationNameEnglish,
            venue: activity_list[i].locationNameEnglish,
            contact: activity_list[i].enquiryContact
        }
        activity.push(x);
}

//console.log(activity);

module.exports = activity;
//activity now contain the list of event details.
