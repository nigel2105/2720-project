// UserSchema.js to Create the User after signing up
var Comment = require('./CommentSchema');
var mongoose = require('mongoose');

var UserSchema = mongoose.Schema({
    username: {type: String, unique: true, required: true},
    pwd: {type: String, unique:true, required:true},
    favEvent: [{type: mongoose.Schema.Types.ObjectId , ref: 'Event'}],
    comments: {type: Number}
},{minimize: false});

var User = mongoose.model('User',UserSchema);

module.exports = User;