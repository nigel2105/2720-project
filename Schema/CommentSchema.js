// CommentSchema.js : the mongoose Schema of Comments

var mongoose = require('mongoose');
var User = require('./UserSchema.js');
var Event = require('./EventSchema.js');

/**
 * @param {string} postedBy
 * @param {string} content
 * @param {Event} event
 * 
 */
var CommentSchema = mongoose.Schema({
    postedBy: {type: String}, // who posted the comment
    content: {type: String},    // what is the comment
    event: {type: mongoose.Schema.Types.ObjectId, ref: 'Event'} // which event is this comment related to
})

var Comment = mongoose.model('Comment', CommentSchema);

module.exports = Comment;