// EventSchema.js : mongoose schema to handle the event
var mongoose = require('mongoose');
var Comment = require('./CommentSchema.js');

var EventSchema =  mongoose.Schema({
    name: {type: String},
    time: {type: String},
    orgName: {type: String},
    venue: {type: String},
    contact: {type: String},
    comments: [{type: mongoose.Schema.Types.ObjectId, ref: 'Comment'}]
},{minimize: false});

var Event = mongoose.model('Event', EventSchema);

module.exports = Event;