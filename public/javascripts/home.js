// script to handle javascript of section home
var URL = 'http://localhost:3000';
$.get(URL, function(data,status){

    $('.username').html(data); // to ensure user always logged in shows the username
}); 

$.get('http://localhost:3000/events', function(data,status){
    // data is the list of events
    let eventnum = data.length;
    let eventstr = eventnum + " events in database";
    $('#stats #events').html(eventstr);
}); // AJAX get /events

$.get('http://localhost:3000/usercomments', function(data,status){
        let commentsnum = data.count;   
        let commentstr = commentsnum + " comments posted";
        
        $('#stats #comments').html(commentstr);
}); //      end of GET /usercomments

$.get('http://localhost:3000/favorites', function(data,status){
        // data: contains the favorite events objectof this user
        
        var size = data.length;
        var favstr = size + " events added to favorite";

        $('#stats #fav').html(favstr);

        // now need to generate all the events

        for(let event of data) {
            let $entry = "<div class='favevent'>" +
                        "<div class ='col-lg-4'>" +
                        "<h4>"  + event.time + "</h4>"+
                        "<h7>"+ event.venue + "</h7>"+ 
                        "</div>"+
                        "<div class = 'col-lg-8'>" + 
                        "<b>" + event.name + "</b> <br />"+
                        "<i class = 'fa fa-male'></i>&nbsp;&nbsp;" + event.orgName + "<br/>" + 
                        "<i class = 'fa fa-phone'></i>&nbsp;&nbsp;" + event.contact +
                        "</div>"+
                        "</div>";
            
            $('#favlist').append($entry);
            
        }


});

// The rest needs to be about logging in the favorites