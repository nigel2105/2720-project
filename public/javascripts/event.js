// event.js : the handler once you load events.html

/// GET request to get user 
var URL = 'http://localhost:3000/events'

$.get(URL, function(data,status){
    // data contains the list of events stored in database
    for(let event of data){
        let $entry = "<tr>" + 
                    "<td onclick='eventDetail(this)' >"+ event.name + "</td>"+
                    "<td>"+ event.time + "</td>"+
                    "<td>"+ event.orgName + "</td>"+
                    "<td>"+ event.venue + "</td>"+
                    "<td>"+ event.contact + "</td>"
                    +"</tr><br\>" ;
        $("#myTable").append($entry);
        

    }// end for>
 
})



/**
 * function eventDetail() => 
 * function related to the handler when each of the event is clicked
 * recognise which event this is
 * store it in req.session
 * upon loading new redirected html use the stored detail in order to easily get the file
 */


function eventDetail(obj){
    // push current to history first:

    var name = $(obj).html();
    var time = $(obj).next().html();
    var orgName = $(obj).next().next().html();
    var venue = $(obj).next().next().next().html();
    var contact = $(obj).next().next().next().next().html();

    var data = {
        name: name,
        time: time,
        orgName: orgName,
        venue: venue,
        contact: contact
    } // end of data

    var url = "http://localhost:3000/click";

    $.post(url, data, function(event,status){
        // after posting we want to change the loaded html file
        // push __id and put as history
        
        var urlstate = 'http://localhost:3000/#eventpage/' + event._id;
        var stateObj =  {page: '#eventpage'}
        history.pushState('stateObj', '', urlstate);

        
        $('section').hide();
        $('#page').load('../HTML/eventdetails.html');
        console.log("succesfully posted");
    });

} // end of function clickEvent