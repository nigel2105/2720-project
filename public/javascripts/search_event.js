// search_event.js : The javascript handler of loading the page
var URL = 'http://localhost:3000/find';

$.get(URL, function(events,status){
    for(let event of events){
        let $entry = "<tr>" + 
                    "<td onclick = 'eventDetail(this)'>"+ event.name + "</td>"+
                    "<td>"+ event.time + "</td>"+
                    "<td>"+ event.orgName + "</td>"+
                    "<td>"+ event.venue + "</td>"+
                    "<td>"+ event.contact + "</td>"
                    +"</tr>";
        $("#myTable").append($entry);
    }
});


function eventDetail(obj){

    

    var name = $(obj).html();
    var time = $(obj).next().html();
    var orgName = $(obj).next().next().html();
    var venue = $(obj).next().next().next().html();
    var contact = $(obj).next().next().next().next().html();

    var data = {
        name: name,
        time: time,
        orgName: orgName,
        venue: venue,
        contact: contact
    } // end of data

    var url = "http://localhost:3000/click";

    $.post(url, data, function(event,status){
        // after posting we want to change the loaded html file
        
        $('section').hide();
        $('#page').load('../HTML/eventdetails.html');
        console.log("succesfully posted");
    });

} // end of function clickEvent