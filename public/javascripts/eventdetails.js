// eventdetails.js : the javascript handler for eventdetails.html (mostly to load the event )

/**
 * get the previously clicked event and get the details later on
 * 
 */
var url = "http://localhost:3000/click";

$.get(url, function(event,status){
    // event contains the found one from the event
    $('#name').html(event.name);
    $('#time').html(event.time);
    $('#orgname').html(event.orgName);
    $('#venue').html(event.venue);
    $('#contact').html(event.contact);


}); // end of get http://localhost:3000/click

var newurl = 'http://localhost:3000/loadcomments';
$.get(newurl, function(comments,status){
    // comments contain all the comments
    for(let comment of comments){
        $newcomment = "<div class='media'>" +"<div class= 'media-body'>" + 
            "<h5 class='media-heading'>" + comment.postedBy + "</h5>"+
            "<p>" + comment.content + "</p>" + "</div>" + "</div>";
        $('#commentlist').append($newcomment);
    } // end for
});  // end of get http://localhost:3000/loadcomments

// The buttton name , need to know where it exists or not
$.get('http://localhost:3000/favbutton', function(data,status){
    if(data == "1") // if found
        $('#favorite').html("Un-favorite this");
    else 
        $('#favorite').html("Add to favorite?");
});

/**
 * handler when you pres submit comment to attach the new comment
 * to page. Then do a POST request to save into comments database
 * @param none
 * @return 
 */
function submitcomment(){
    //get the comment
    $comment = $('textarea').val()
    var url = 'http://localhost:3000/addcomment';

    var data = {
        content : $comment
    }; // content of data passing through

    $.post(url, data,function(username,status){
        $newcomment = "<li id=\"commentlist\" style=\"padding-left: 5px;border-radius: 10px;box-shadow: 0.5px 0.5px 0.5px #888888 \">" 
        "<h5>" + username + "</h5>"+
            "<p>" + $comment + "</p>" + "</li><br\\><br\\>";
    $('#commentlist').append($newcomment);
    $('textarea').val('');
    }); // end of post request ajax



} // end of function submitcomment

function favcomment(){
    // simply do a get request
    var url = 'http://localhost:3000/fav';
    
    $.get(url, function(data,status){
        alert(data);
    }); // get request
} // end of function favcomment