// search_form.js : The javascript file to handle 
function search(){
    
    let field = $('#efield').val();
    
    let keyword = $("input[id='ekeyword'").val();

   
    var data = {
        field: field,
        keyword: keyword

    };
    
    $.post('http://localhost:3000/find', data, function(event,status){
        // alert(event);

        $("tr[name*=temp]").remove();
        $("p[name*=temp]").remove();
        if(event.length < 1){
            $('#retrieve').prepend('<p name="temp" class="lead">' + event.length + ' event(s) found</p>');
            return;
        }
        var i;
        var t = "<tr style=\"background-color: lightgrey\" name="+'"temp"'+"><th onclick=\"sortField(0)\">Name &nbsp;&nbsp;&nbsp;&nbsp;<i id=\"name\" class=\"fa fa fa-sort\"></i></th><th onclick=\"sortField(1)\">Time &nbsp;&nbsp;&nbsp;&nbsp;<i id=\"time\" class=\"fa fa fa-sort\"></i></th><th onclick=\"sortField(2)\">Organization Name &nbsp;&nbsp;&nbsp;&nbsp;<i id=\"orgname\" class=\"fa fa fa-sort\"></i></th><th onclick=\"sortField(3)\">Venue &nbsp;&nbsp;&nbsp;&nbsp;<i id=\"venue\" class=\"fa fa fa-sort\"></i></th><th onclick=\"sortField(4)\">Contact &nbsp;&nbsp;&nbsp;&nbsp;<i id=\"contact\" class=\"fa fa fa-sort\"></i></tr>";
        for (i = 0; i < event.length; i++)  
        { 
            t = t + "<tr id= "+'"'+event[i]._id +'"'+"name="+ '"temp"' +"><td>" + event[i].name + 
            "</td><td>" + event[i].time + 
            "</td><td>" + event[i].orgName + 
            "</td><td>" + event[i].venue + 
            "</td><td>" + event[i].contact
        } 
        $('#retrieve').prepend('<p name="temp" class="lead">' + event.length + ' event(s) found</p>');
        $('#retrieve').append(t);
    });
    
}

/* Nic's Implementation
function search(){
    let field = $("input[name='field']").val();
    let keyword = $("input[name='keyword']").val();
    var URL = 'http://localhost:3000/find'; // URL for post request (found in routing.js handler)

    var data ={
        field : field,
        keyword: keyword
    };

    $.post(URL, data, function(events,status){
        // data should be response data ( array)
        var stateObj = {page: '#searchpage'};
        history.pushState(stateObj,'', 'http://localhost:3000/#searchpage?field='+data.field+'&keyword='+data.keyword);
        $('section').remove(); // firstly, delete section
       //$('#page').load('../HTML/search_event.html');


        
    }); // end of post 

}*/
