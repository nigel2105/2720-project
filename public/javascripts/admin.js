function eventSearchForm(){
    $('#page').load('../HTML/admin_event_search_form.html');
}

function userSearchForm(){
    $('#page').load('../HTML/admin_user_search_form.html');
}

function eventCreateForm(){
    $('#page').load('../HTML/admin_event_create_form.html');
}

function userCreateForm(){
    $('#page').load('../HTML/admin_user_create_form.html');
}

function postCreateEvent(){
    // get content
    let name = $("input[id='ename'").val();

    let time = $("input[id='etime'").val();

    let orgName = $("input[id='eorg'").val();

    let venue = $("input[id='evenue'").val();

    let contact = $("input[id='econtacts'").val();

   
    var data = {
        name: name,
        time: time,
        orgName: orgName,
        venue: venue,
        contact: contact

    };

    $.post('http://localhost:3000/events', data, function(events,status){
        alert("Created Data: "+ events.name);
        // alert(events);
    });
    
}

function postRetrieveEvent(){
    
    let field = $('#efield').val();
    
    let keyword = $("input[id='ekeyword'").val();

   
    var data = {
        field: field,
        keyword: keyword

    };
    
    
    $.post('http://localhost:3000/findEvent', data, function(event,status){
        // alert(event);
        $("tr[name*=temp]").remove();
        $("p[name*=temp]").remove();
        var i;
        var t = "<tr style=\"background-color: lightgrey\" name="+'"temp"'+"><th onclick=\"sortField(0)\">Name &nbsp;&nbsp;&nbsp;&nbsp;<i id=\"name\" class=\"fa fa fa-sort\"></i></th><th onclick=\"sortField(1)\">Time &nbsp;&nbsp;&nbsp;&nbsp;<i id=\"time\" class=\"fa fa fa-sort\"></i></th><th onclick=\"sortField(2)\">Organization Name &nbsp;&nbsp;&nbsp;&nbsp;<i id=\"orgname\" class=\"fa fa fa-sort\"></i></th><th onclick=\"sortField(3)\">Venue &nbsp;&nbsp;&nbsp;&nbsp;<i id=\"venue\" class=\"fa fa fa-sort\"></i></th><th onclick=\"sortField(4)\">Contact &nbsp;&nbsp;&nbsp;&nbsp;<i id=\"contact\" class=\"fa fa fa-sort\"></i></th><th></th><th></th></tr>";
        for (i = 0; i < event.length; i++)  
        { 
            t = t + "<tr id= "+'"'+event[i]._id +'"'+"name="+ '"temp"' +"><td>" + event[i].name + 
            "</td><td>" + event[i].time + 
            "</td><td>" + event[i].orgName + 
            "</td><td>" + event[i].venue + 
            "</td><td>" + event[i].contact + 
            "</td><td>" + '<button id="submitE'+i+'" class="btn btn-success" type="button" onclick="putUpdateEventForm('+"'"+event[i]._id+"'"+')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>'+
            "</td><td>" + '<button id="deleteE'+i+'" class="btn btn-danger" type="button" onclick="postDeleteEvent('+"'"+event[i]._id+"'"+')"><i class="fa fa-trash-o"></i></button>'+ 
            "</td></tr>";
        } 
        $('#retrieve').prepend('<p name="temp" class="lead">' + event.length + ' event(s) found</p>');
        $('#retrieve').append(t);
    });
    
}


function putUpdateEventForm(_id){
    // alert("Updating event with id: "+_id);
    
    $("tr[name*=tempForm]").remove();
    let t = $('#'+_id).html();
    var i = 0;
    var td = ["","","","","",""];
    $(t).filter("td").each(function(){
        td[i] = $(this).text();
        i = i + 1;
    });
    let name = td[0];
    let time = td[1];
    let orgName = td[2];
    let venue = td[3];
    let contact = td[4];
// '<input type="text" class="form-control" id="euname">'
    var x = "";
    x = "<tr name="+ '"tempForm">' + "<form><td>"+
    '<input type="text" class="form-control" id="euname" value="'+name+'"></td><td>'+
    '<input type="text" class="form-control" id="eutime" value="'+time+'"></td><td>'+
    '<input type="text" class="form-control" id="euorgname" value="'+orgName+'"></td><td>'+
    '<input type="text" class="form-control" id="euvenue" value="'+venue+'"></td><td>'+
    '<input type="text" class="form-control" id="eucontact" value="'+contact+'"></td><td>'+
    '<button class="btn btn-success" type="button" onclick="putUpdateEvent('+"'"+_id+"'"+')">Update!</button>'+"</form></td></tr>";
    $('#'+_id).after(x);

    
}

function putUpdateEvent(_id){

    let name = $("input[id='euname'").val();

    let time = $("input[id='eutime'").val();

    let orgName = $("input[id='euorgname'").val();

    let venue = $("input[id='euvenue'").val();

    let contact = $("input[id='eucontact'").val();

   
    var data = {
        _id: _id,
        name: name,
        time: time,
        orgName: orgName,
        venue: venue,
        contact: contact

    };
    
    $.post('http://localhost:3000/updateEvents', data, function(event, status){
        alert("Successfully updated data.");
    });
}

function postDeleteEvent(_id){
   
    var data = {
        _id: _id

    };
    
    $.post('http://localhost:3000/deleteEvent', data, function(event, status){
        alert("Successfully deleted data.");
    });
}

function postCreateUser(){
    // get content
    let username = $("input[id='uname'").val();

    let pwd = $("input[id='upwd'").val();

   
    var data = {
        username: username,
        password: pwd

    };
        
    $.post('http://localhost:3000/signup', data, function(data,status){
        alert("User Successfully Created!");
    });
    
}
function postRetrieveUser(){
    // get content
    let field = "username"

    let keyword = $("input[id='urname'").val();

   
   
    var data = {
        field: field,
        keyword: keyword

    };
        
   $.post('http://localhost:3000/findUser', data, function(user,status){
        if(user.length < 1){
            alert("User Not Found!");
            return;
        }
        //alert("Number of Retrieved Data: "+ user.length);
        $("tr[name*=temp]").remove();
        $('#temp').remove(); 
        var i;
        var t = "<tr style=\"background-color: lightgrey\" name="+'"temp"'+"><th>Name</th><th>ID</th><th>Password</th><th></th><th></th></tr>";
        for (i = 0; i < user.length; i++)  
        { 
            t = t + "<tr id= "+'"'+user[i]._id +'"'+"name="+ '"temp"' +"><td>" + user[i].username + 
            "</td><td>" + user[i]._id + 
            "</td><td>" + user[i].pwd + 
            "</td><td>" + '<button id="submitU'+i+'" class="btn btn-success" type="button" onclick="putUpdateUserForm('+"'"+user[i]._id+"'"+')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>'+ 
            "</td><td>" + '<button id="deleteU'+i+'" class="btn btn-danger" type="button" onclick="postDeleteUser('+"'"+user[i]._id+"'"+')"><i class="fa fa-trash-o"></i></button>'+ 
            "</td></tr>";
        } 
        $('#retrieve').append(t);
    });
    
}
function putUpdateUserForm(_id){
    // alert("Updating event with id: "+_id);
    
    $("tr[name*=tempForm]").remove();
    let t = $('#'+_id).html();
    var i = 0;
    var td = ["","",""];
    $(t).filter("td").each(function(){
        td[i] = $(this).text();
        i = i + 1;
    });
    

    let username = td[0];
    let pwd = td[2];
    
// '<input type="text" class="form-control" id="euname">'
    var x = "";
    x = "<tr name="+ '"tempForm">' + "<form><td>"+
    '<input type="text" class="form-control" id="uuname" value="'+username+'"></td><td>'+
    "Can't change ID!</td><td>"+
    '<input type="text" class="form-control" id="uutime" value="'+pwd+'"></td><td>'+
    '<button class="btn btn-success" type="button" onclick="putUpdateUser('+"'"+_id+"'"+')">Update!</button>'+"</form></td></tr>";
    $('#'+_id).after(x);

    
}

function putUpdateUser(_id){

    let username = $("input[id='uuname'").val();

    let pwd = $("input[id='uutime'").val();

    

   
    var data = {
        _id: _id,
        username: username,
        pwd: pwd
    };
    
    $.post('http://localhost:3000/updateUsers', data, function(user, status){
        alert("Successfully updated data.");
    });
}

function postDeleteUser(_id){
   
    var data = {
        _id: _id

    };
    
    $.post('http://localhost:3000/deleteUser', data, function(user, status){
        alert("Successfully deleted data.");
    });
}


// The Sorting Function
var prevFilter = -1;
function sortField(n) {
    if(prevFilter!=n){
      if (prevFilter==0){
          document.getElementById("name").classList.remove("fa-spin");
          document.getElementById("name").style.color = "black";
      }
      if (prevFilter==1){
          document.getElementById("time").classList.remove("fa-spin");
          document.getElementById("time").style.color = "black";
      }
      if (prevFilter==2){
          document.getElementById("orgname").classList.remove("fa-spin");
          document.getElementById("orgname").style.color = "black";
      }
      if (prevFilter==3){
          document.getElementById("venue").classList.remove("fa-spin");
          document.getElementById("venue").style.color = "black";
      }
      if (prevFilter==4){
          document.getElementById("contact").classList.remove("fa-spin");
          document.getElementById("contact").style.color = "black";
      }
    }

    if (n==0){
      document.getElementById("name").classList.add("fa-spin");
      document.getElementById("name").style.color = "red";
    }
    if (n==1){
      document.getElementById("time").classList.add("fa-spin");
      document.getElementById("time").style.color = "red";
    }
    if (n==2){
      document.getElementById("orgname").classList.add("fa-spin");
      document.getElementById("orgname").style.color = "red";
    }
    if (n==3){
      document.getElementById("venue").classList.add("fa-spin");
      document.getElementById("venue").style.color = "red";
    }
    if (n==4){
      document.getElementById("contact").classList.add("fa-spin");
      document.getElementById("contact").style.color = "red";
    }
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("myTable");
    switching = true;
    //Set the sorting direction to ascending:
    dir = "asc"; 
    /*Make a loop that will continue until
    no switching has been done:*/
    while (switching) {
      //start by saying: no switching is done:
      switching = false;
      rows = table.rows;
      /*Loop through all table rows (except the
      first, which contains table headers):*/
      for (i = 1; i < (rows.length - 1); i++) {
        //start by saying there should be no switching:
        shouldSwitch = false;
        /*Get the two elements you want to compare,
        one from current row and one from the next:*/
        x = rows[i].getElementsByTagName("TD")[n];
        y = rows[i + 1].getElementsByTagName("TD")[n];
        /*check if the two rows should switch place,
        based on the direction, asc or desc:*/
        if (dir == "asc") {
          if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
            //if so, mark as a switch and break the loop:
            shouldSwitch= true;
            break;
          }
        } else if (dir == "desc") {
          if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
            //if so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
        }
      }
      if (shouldSwitch) {
        /*If a switch has been marked, make the switch
        and mark that a switch has been done:*/
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
        //Each time a switch is done, increase this count by 1:
        switchcount ++;      
      } else {
        /*If no switching has been done AND the direction is "asc",
        set the direction to "desc" and run the while loop again.*/
        if (switchcount == 0 && dir == "asc") {
          dir = "desc";
          switching = true;
        }
      }
    }
  prevFilter = n;  
  }