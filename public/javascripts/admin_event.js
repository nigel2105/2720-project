function postCreateEvent(){
    // get content
    let name = $("input[id='ename'").val();

    let time = $("input[id='etime'").val();

    let orgName = $("input[id='eorg'").val();

    let venue = $("input[id='evenue'").val();

    let contact = $("input[id='econtacts'").val();

   
    var data = {
        name: name,
        time: time,
        orgName: orgName,
        venue: venue,
        contact: contact

    };
    

    
    
    $.post('http://localhost:3000/events', data, function(events,status){
        alert("Created Data: "+ events.name);
        // alert(events);
    });
    
}

function postRetrieveEvent(){
    
    let field = $('#efield').val();
    
    let keyword = $("input[id='ekeyword'").val();

   
    var data = {
        field: field,
        keyword: keyword

    };
    
    
    $.post('http://localhost:3000/findEvent', data, function(event,status){
        // alert(event);
        alert("Number of Retrieved Data: "+ event.length);
        $("tr[name*=temp]").remove();
        var i;
        var t = "<tr name="+'"temp"'+"><th>Name</th><th>Time</th><th>Organization Name</th><th>Venue</th><th>Contact</th>";
        for (i = 0; i < event.length; i++)  
        { 
            t = t + "<tr id= "+'"'+event[i]._id +'"'+"name="+ '"temp"' +"><td>" + event[i].name + 
            "</td><td>" + event[i].time + 
            "</td><td>" + event[i].orgName + 
            "</td><td>" + event[i].venue + 
            "</td><td>" + event[i].contact + 
            "</td><td>" + '<button id="submitE'+i+'" class="signupbtn" type="button" onclick="putUpdateEventForm('+"'"+event[i]._id+"'"+')">Update Form</button>'+
            "</td><td>" + '<button id="deleteE'+i+'" class="signupbtn" type="button" onclick="postDeleteEvent('+"'"+event[i]._id+"'"+')">Delete!</button>'+ 
            "</td></tr>";
        } 
        $('#retrieve').append(t);
    });
    
}

function putUpdateEventForm(_id){
    // alert("Updating event with id: "+_id);
    
    $("tr[name*=tempForm]").remove();
    let t = $('#'+_id).html();
    var i = 0;
    var td = ["","","","","",""];
    $(t).filter("td").each(function(){
        td[i] = $(this).text();
        i = i + 1;
    });
    let name = td[0];
    let time = td[1];
    let orgName = td[2];
    let venue = td[3];
    let contact = td[4];
// '<input type="text" class="form-control" id="euname">'
    var x = "";
    x = "<tr name="+ '"tempForm">' + "<form><td>"+
    '<input type="text" class="form-control" id="euname" value="'+name+'"></td><td>'+
    '<input type="text" class="form-control" id="eutime" value="'+time+'"></td><td>'+
    '<input type="text" class="form-control" id="euorgname" value="'+orgName+'"></td><td>'+
    '<input type="text" class="form-control" id="euvenue" value="'+venue+'"></td><td>'+
    '<input type="text" class="form-control" id="eucontact" value="'+contact+'"></td><td>'+
    '<button class="signupbtn" type="button" onclick="putUpdateEvent('+"'"+_id+"'"+')">Update!</button>'+"</form></td></tr>";
    $('#'+_id).after(x);

    
}

function putUpdateEvent(_id){

    let name = $("input[id='euname'").val();

    let time = $("input[id='eutime'").val();

    let orgName = $("input[id='euorgname'").val();

    let venue = $("input[id='euvenue'").val();

    let contact = $("input[id='eucontact'").val();

   
    var data = {
        _id: _id,
        name: name,
        time: time,
        orgName: orgName,
        venue: venue,
        contact: contact

    };
    
    $.post('http://localhost:3000/updateEvents', data, function(event, status){
        alert("Successfully updated data.");
    });
}

function postDeleteEvent(_id){
   
    var data = {
        _id: _id

    };
    
    $.post('http://localhost:3000/deleteEvent', data, function(event, status){
        alert("Successfully deleted data.");
    });
}

