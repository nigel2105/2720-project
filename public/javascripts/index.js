// index.js: used to handle the index.html file for changing sites

// store the tab youre in 
$(document).ready(function(){

    let x = {page: '#home'};
    history.pushState(x,'','/#home');
    var URL = 'http://localhost:3000/';
    $.get(URL, function(data,status){

        $('.username').html(data);
    }); // end of ajax get


$('[href^="#"]').on('click', function(e) {


    let title = "";
    let href =  $(this).attr('href');
    let stateObj = {page: href};
    console.log(href); // x contains something like : #eventpage, #homepage, etc.
    var URL = 'http://localhost:3000/' + href;

    history.pushState(stateObj, title, URL);

    return e.preventDefault();

});

window.onpopstate = function(event){
    href = event.state.page;
    var str = href.slice(1);
    console.log(str); // str must be either eventpage, home, searchpage

    // if cases 1 by 1
    if(str == 'home') homepage();
    else if(str == 'searchpage') searchpage();
    else if(str == 'eventpage') eventpage();
    else if(str == 'about') about();
    else console.log("none of the result ");

}

// Showing user statistics include:
// number of events available, number of comments, and number of favorite events

// to get number of events
$.get('http://localhost:3000/events', function(data,status){
    // data is the list of events
    let eventnum = data.length;
    let eventstr = eventnum + " events in database";
    $('#stats #events').html(eventstr);
}); // AJAX get /events

$.get('http://localhost:3000/usercomments', function(data,status){
        let commentsnum = data.count;   
        let commentstr = commentsnum + " comments posted";
        
        $('#stats #comments').html(commentstr);
}); //      end of GET /usercomments

$.get('http://localhost:3000/favorites', function(data,status){
        // data: contains the favorite events objectof this user
        
        var size = data.length;
        var favstr = size + " events added to favorite";

        $('#stats #fav').html(favstr);

        // now need to generate all the events

        for(let event of data) {
            let $entry = "<div class='favevent'>" +
                        "<div class ='col-lg-4'>" +
                        "<h5>"  + event.time + "</h5>"+
                        "<h7>"+ event.venue + "</h7>"+ 
                        "</div>"+
                        "<div class = 'col-lg-8'>" + 
                        "<b>" + event.name + "</b> <br />"+
                        "<i class = 'fa fa-male'></i>&nbsp;&nbsp;" + event.orgName + "<br/>" + 
                        "<i class = 'fa fa-phone'></i>&nbsp;&nbsp;" + event.contact +
                        "</div>"+
                        "</div>";
            
            $('#favlist').append($entry);
            
        }


}); //       end of GET /favorites


});


/**
 * function to return back to homepage
 * @param {*} e 
 */
function homepage() {
    $("#section").remove();
    $('#page').load('../HTML/home.html');
}


/**
 * function event
 * remove the current section then add the new section
 */
function eventpage(){
    
    $("section").remove();
    $('#page').load('../HTML/events.html');
    

    
}// after clicking event button of navbar


/**
 * redirecting to searchpage
 */
function searchpage(){

    $("#section").hide();
    $('#page').load('../HTML/search_form.html');
}

/**
 * logging out after pressing username
 */
function logout(){
    var URL = 'http://localhost:3000/logout';
    alert(history.state);
    $.get(URL, function(data ,status){
        $('html').html(data);
    }); //       end of AJAX get
}

function about(){
    $('#page').load('../HTML/about.html');
    
}


