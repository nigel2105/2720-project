var express = require('express');
var session = require('express-session');
var app = express();
var bodyParser = require('body-parser');
var User = require('./Schema/UserSchema');
var urlEncodedParser = bodyParser.urlencoded({extended: false});
var mongoose = require('mongoose');
var cors = require('cors');
var Comment = require('./Schema/CommentSchema');
var Event = require('./Schema/EventSchema');
<<<<<<< HEAD
var multer = require('multer');
var upload = multer({ dest: 'uploads/' });

=======
var request = require('request');
>>>>>>> afa30132001140c64c172e6f74b89a7819794dbf


var bcrypt = require('bcryptjs');       // bcrypt.hashSync() to has password and bcrypt.compareSync() to compare hashed password
app.use(urlEncodedParser);
var cookieParser = require('cookie-parser');


mongoose.connect('mongodb://nigel:nigel@localhost/project');
var db = mongoose.connection;


db.on('error', console.error.bind(console, 'Connection error:'));
db.once('open', function(){
    console.log("Database connected");
})

app.use(cookieParser());

app.use(express.static('public'));

app.use('css', express.static("/public/CSS"));
app.use("/scripts",express.static("/public/javascripts"));
app.use(cors());

//session handling
app.use(session({
    secret: '123456',
    cookie: { secure:false},
    resave: false,
    saveUninitialized: false
}));


/**
 * Function GET / =>
 * return the session user that is currently logged in
 */
app.get('/',function(req,res){
    console.log('GET/ received');
    console.log(req.session.user.username);
    res.send(req.session.user.username);
});



/**
 * Function GET /delete =>
 * destorys the current session ( done when you are logged out)
 */
app.get("/logout", function(req,res){    //delete session
    req.session.destroy();
    console.log("logging out");
    res.redirect('/signin');
    req.session.view = 0 ;
    //res.send("Session destroyed");
})

app.get('/loadcomments', function(req,res){
    var event = req.session.clickevent;
    Event
    .findById(event._id)
    .populate('comments')
    .exec(function(err, x){
        
        console.log(x.comments.postedBy);
        res.send(x.comments);
    }); // end of exec
    

}); // end of GET /loadcomments


/**
 * GET /signup -> to give the html file
 */
app.get('/signup', function(req,res){   
    res.sendFile(__dirname+'/public/HTML/signup.html');
});


app.get('/flush', function(req,res){
    request('http://fundraising.one.gov.hk/fundraise_query/webservice/psi/json', function(err, response, data){
        let list = JSON.parse(data);
        var activities = list.activities;
        var activityarray = Array();

        for(i=0;i<activities.length;i++){
            var x ={
                name: activities[i].activityNameEnglish,
                time: activities[i].schedule[0].dateFrom +" " +activities[i].schedule[0].timeFrom,  // for date_and_time, we choose the latest event
                orgName: activities[i].organisationNameEnglish,
                venue: activities[i].locationNameEnglish,
                contact: activities[i].enquiryContact
            }
            // CHECK WITH DATABASE HERE , SEE IF THIS KIND OF EVENT ALREADY EXIST OR NOT
            
            
            
            activityarray.push(x);
        }

        Event
        .find({})
        .exec(function(err, events){
            // check for all events with everything in x 
            if(events.length< 1) {  //          if it contains nothing then just insert everything into it( for initial in example )
                Event.insertMany(activityarray, function(err,act){
                    if(err) console.log(err);
                    console.log(act);
                });
            } //        end if length<1

            else {  //          else check for all in activity array compared to 
                for(i=0; i<activityarray.length; i++){
                    var found = 0;
                    for(j=0; j<events.length; j++){
                        if(events[j].time == activityarray[i].time && events[j].orgName == activityarray[i].orgName ) {found=1;console.log("Found!");}

                    }
                    if(!found) {   //         if not found, insert to database
                        console.log("Not foun!");
                        var newdata = new Event({
                            name: activityarray[i].name,
                            time: activityarray[i].time,
                            orgName: activityarray[i].orgName,
                            venue: activityarray[i].venue,
                            contact: activityarray[i].contact
                        });

                        newdata.save(function(err){
                            if(err) console.log(err);
                        })
                    }   //  end if found =0 
                }
                
            }

            

        })
        
        

    


        
        /*Event.insertMany(activityarray, function(err,act){
            if(err) console.log(err);
            console.log(act);
        });*/

        res.send(activityarray);
        
    });
});

/**
 * Function POST /singup
 * action upon signing up through a form 
 * and store it into database
 */
app.post('/signup', function(req,res){
    console.log("POST REQUEST CREATED");

    var username = req.body['username'];
    var pwd = req.body['password'];


    var user = new User({
        username: username,
        pwd: pwd
    });



    user.save(function(err){
        if(err)
            {
                console.log("ERROR");

                res.send(null);
                
            }
            else {
                res.status(200);
                res.send(user);
            }
    });

    //res.send(user);
});

/**
 * Function POST /login
 * action when you signin through form in http://localhost:3000/signin
 */
app.post('/login', function(req,res){
    console.log("POST/login created");
    var username = req.body['username'];
    var pwd = req.body['password'];

    User
    .findOne({username: username})
    .exec(function(err, user){
        if(!user) {
            res.send(null);
        }
        else {
            // if user found, check if password match

            // still some error here (bcrypt.compareSync)
            if(pwd == user.pwd)
                {
                    req.session.user = user;
                    console.log(req.session.user);
                    res.sendFile(__dirname+ '/public/HTML/index.html')

            }
            else 
                res.send("Account Does not Exist, try again!");

        }

    })


});//end of handler


/**
 * Function GET /index =>
 * load the index.html which is the home page
 */
app.get('/index',function(req,res){
    res.sendFile(__dirname + '/public/HTML/index.html');
});

/**
 * Function GET /signin =>
 * http://localhost:3000/signin 
 */
app.all('/signin', function(req,res){
    req.session.page_view = 1;


    res.sendFile(__dirname +'/public/HTML/signin.html');

}); // end of handler

// file upload
app.post('/upload', function (req, res)
{
    var upload_myfile = upload.single('myfile');   

    upload_myfile(req, res, function(err){
        var file = req.body[0].path;

        var fs = require('fs');
     
        var csv = fs.readFileSync(file, 'utf8');
        var data = $.csv.toObjects(csv);
        Event.insertMany(data, function(err){
            if (err)
                res.send(err);
        })
        res.send();   
    })
    
});
/**
 * Function GET /events =>
 * returns all list of events
 */
app.get('/events', function(req,res){
    var user = req.session.user;

    Event
    .find({})    
    .exec(function(err, events){
        res.send(events);
    });

});

app.post('/events', function(req,res){
    console.log("POST REQUEST CREATED");

    var name = req.body['name'];
    var time = req.body['time'];
    var orgName = req.body['orgName'];
    var venue = req.body['venue'];
    var contact = req.body['contact'];


    var event = new Event({
        name: name,
        time: time,
        orgName: orgName,
        venue: venue,
        contact: contact
    });

    event.save(function(err){
        if(err)
            res.send(err);
    });

    res.send(event);
});



// app.post('/csvevents', function(req,res){
//     console.log("csv");

    
// })
//  end of handler /events


// update 
app.post('/updateEvents', function(req, res){
    console.log("UPDATE REQUEST CREATED");

    var _id = req.body['_id'];
    var name = req.body['name'];
    var time = req.body['time'];
    var orgName = req.body['orgName'];
    var venue = req.body['venue'];
    var contact = req.body['contact'];

    var conditions = {_id: _id};
    

    Event.findOne(conditions, function(err, event){
        if(err){
            console.log(err);
            return handleError(err);
        }
        if(event != null){
            event.name = name;
            event.time = time;
            event.orgName = orgName;
            event.venue = venue;
            event.contact = contact;
            event.save();
            res.send();
        }

    });

})

app.post('/updateUsers', function(req, res){
    console.log("UPDATE REQUEST CREATED");

    var _id = req.body['_id'];
    var username = req.body['username'];
    var pwd = req.body['pwd'];

    var conditions = {_id: _id};
    

    Event.findOne(conditions, function(err, user){
        if(err){
            console.log(err);
            return handleError(err);
        }
        if(user != null){
            user.username = username;
            user.pwd = pwd;
            user.save();
            res.send()
        }

    });

})


// Delete 
app.post('/deleteEvent', function(req, res){
    console.log("DELETE REQUEST CREATED");

    var _id = req.body['_id'];

    var conditions = {_id: _id};
    

    Event.deleteOne(conditions, function(err, event){
        console.log(event);
        res.send();
    });

})
app.post('/deleteUser', function(req, res){
    console.log("DELETE REQUEST CREATED");

    var _id = req.body['_id'];

    var conditions = {_id: _id};
    

    User.deleteOne(conditions, function(err, user){
        console.log(user);
        res.send();
    });

})

/**
 * Function POST /find =>
 * To search for event(s) w/ the selected field and keyword
 * given that the keyword deals with regex
 */
app.post('/find', function(req,res){
    var field = req.body['field'];
    var keyword = req.body['keyword'];

    var query = Event.find({});

    query
    .where(field).regex(new RegExp(keyword,'i'))
    .exec(function(err,event){
        console.log(event);
        req.session.data = event;
        res.send(event);
    })
    
    
});

app.post('/findEvent', function(req,res){
    var field = req.body['field'];
    var keyword = req.body['keyword'];

    var query = Event.find({});

    query
    .where(field).regex(new RegExp(keyword,'i'))
    .exec(function(err,event){
        console.log(event);
        // req.session.data = event;
        res.send(event);  
    })
    
    
});

app.post('/findUser', function(req,res){
    var field = req.body['field'];
    var keyword = req.body['keyword'];

    var query = User.find({});

    query
    .where(field).regex(new RegExp(keyword,'i'))
    .exec(function(err,user){
        console.log(user);
        // req.session.data = user;
        res.send(user);
    })
    
    
}); // end of handler POST /find

/**
 * Handler to return the found search
 */
app.get('/find', function(req,res){
    res.send(req.session.data);
}); // handler for GET /find



/**
 * Function POST /click =>
 * handler is supposed to find the corresponding event from given things
 * and then save it in req.session so that you can retrieve it with get later on
 * 
 */
app.post('/click', function(req,res){
    Event
    .findOne( { 
        name: req.body['name'],
        time: req.body['time'],
        orgName: req.body['orgName'],
        venue: req.body['venue'],
        contact: req.body['contact']
    }) // end of find
    .exec(function(err, event){
        req.session.clickevent = event;
        console.log(req.session.clickevent);
        res.send(event);
    }) // end of exec
}); // end handler

/**
 * Function POST /addcomment =>
 * we can use the existing:
 * req.session.user and req.session.clickevent
 */
app.post('/addcomment', function(req,res){

    var comment = new Comment({
        postedBy: req.session.user.username,
        content: req.body['content'],
        event: req.session.clickevent
    });
    var event = req.session.clickevent;

    Event
    .findById(event._id)
    .exec(function(err,x){
        x.comments.push(comment);
        x.save(function(err,updated){
            if(err) return handleError(err);

        });
    });
    

    comment.save(function(err){
        if(err)
            res.send(err);
    });


    console.log("POST /addcomment ");
    res.send(req.session.user.username); // so that they can do something with response
    
});

/**
 * Function get /click =>
 * handler to retrieve the the req.session.clickevent
 * knowing which event did you click
 */
app.get('/click', function(req,res){
    res.send(req.session.clickevent);
}); // end handler

app.get('/fav', function(req,res){
    var user = req.session.user;
    // if the event exists in list of favorite events, then remove it
    // else add the event to the list
    var event = req.session.clickevent;
    User
    .findById(user._id)
    .populate('favEvent')
    .exec(function(err,found){
        var y = 0;
        for( let a of found.favEvent){
            if(a._id == event._id) y = 1;
        }
        if(y){ // if found remove it
            found.favEvent.pull(event);
            found.save(function(err){
                if(err) return handleError(err);
            })
            console.log("Pulled out!");
        }
        else {
            found.favEvent.push(event);
            found.save(function(err){
                if(err) return handleError(err);
            })
            console.log("Pushed in");
        }

        
    }); // end of exec
    
}); // end handler

app.get('/favbutton', function(req,res){
    var user = req.session.user;
    var event = req.session.clickevent;
    console.log("GET /favbutton")
    User
    .findById(user._id)
    .populate('favEvent')
    .exec(function(err,x){
        console.log("------");
        console.log(x.favEvent);
        console.log(event);
        var y = 0;
        for(let a of x.favEvent){
            console.log(a._id);
            console.log(event._id);
            if(a._id == event._id){
                console.log("FOUND!");
                y = 1;
            }
        }
        if(y) res.send("1");
        else res.send("0");
    }); // end of exec

}); // end of GET /favbutton

/**
 *   handler to see number of comments posted
 */
app.get('/usercomments', function(req,res){
    var user = req.session.user;
    var count = 0;
    Event
    .find({})
    .populate('comments')
    .exec(function(err, events){
        console.log('/USERCOMMENTS');
        // events contain the list of all events that exist
        console.log(events);
        for(let event of events) {
            for(let comment of event.comments){
                if(comment.postedBy == user.username) count = count+1;
            }
         }//        end for loop
         res.json({count: count});
    });

}); //          end of GET /usercomments


/**
 * handler to return the favorite events of this current user
 */
app.get('/favorites', function(req,res){
    var user = req.session.user;
    User
    .findById(user)
    .populate('favEvent')
    .exec(function(err,user){
        // user = the type user
        res.send(user.favEvent);
    });

}); //          end of GET favorites

// For Admin Access
app.get('/admin', function(req,res){
    res.sendFile(__dirname+'/public/HTML/admin.html');
});

app.all('/*', function(req,res){
    
    if(typeof req.session.view === "undefined") {
        res.redirect('/signin'); 
        
    }
    else {
        res.sendFile(__dirname + '/public/HTML/index.html');
    }

}); //          end of request ALL 


var server = app.listen(3000);